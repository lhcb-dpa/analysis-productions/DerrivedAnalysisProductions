__all__ = ("wrapper",)

import atexit

def store_metadata():
    import json
    from pathlib import Path
    if hasattr(wrapper, "_metadata"):
        metadata = list(wrapper._metadata.values())
        Path("metadata.json").write_text(json.dumps(metadata))
    else:
        print("WARNING: No metadata detected")


atexit.register(store_metadata)


def wrapper(data, /, **kwargs):
    import inspect
    from itertools import chain
    from pathlib import Path
    from collections import defaultdict

    import snakemake

    stack = inspect.stack()
    frame_idx = 1
    dap_base = Path("/home/cburr/DerrivedAnalysisProductions/")
    output_bases = {
        "v0r0p5354949": Path("/eos/lhcb/wg/dpa/wp2/recompress-v0r0p5354949/"),
        "v0r0p5469954": Path("/eos/lhcb/wg/dpa/wp2/recompress-v0r0p5469954/"),
    }

    # Get the wildcards of the input lambda function
    assert stack[frame_idx].function == '<lambda>'
    arg_info = inspect.getargvalues(stack[frame_idx].frame)
    assert len(arg_info.args) == 1, "TODO"
    wildcards = arg_info.locals[arg_info.args[0]]
    assert isinstance(wildcards, snakemake.io.Wildcards)

    # Get the current rule
    arg_info = inspect.getargvalues(stack[frame_idx+1].frame)
    rule_ = arg_info.locals[arg_info.args[0]]
    assert isinstance(rule_, snakemake.rules.Rule)

    # Get infomation about the current rule
    assert len(rule_.input) == 1, "TODO"
    assert all(isinstance(o, str) for o in rule_.output), "TODO"
    output_files = {
        k: str(Path(v.format(**wildcards)).relative_to(output_bases[wildcards.version]))
        for k, v in rule_.output.items()
    }
    assert len(rule_.output) == len(output_files), "All output files must be named!"

    pfns = data(**kwargs)

    # Get the input LFNs
    effective_tags = data.analysisData._default_tags | kwargs
    samples = data.analysisData._samples.filter(**effective_tags)
    if samples.PFNs() != pfns:
        raise NotImplementedError("This shouldn't happen")
    input_lfns = list(chain(*(s["lfns"] for s in samples)))

    # Find the common tags between all samples
    common_tags = defaultdict(set)
    for s in samples.info:
        for k, v in samples._sampleTags(s).items():
            if k not in {"name", "version", "state"}:
                common_tags[k].add(v)
    common_tags = {k: v.pop() for k, v in common_tags.items() if len(v) == 1}

    # Store the metadata
    rule_info = {
        "request_id": [s["request_id"] for s in samples],
        "snakefile": str(Path(rule_.snakefile).relative_to(dap_base)),
        "rule": rule_.name,
        "wildcards": dict(wildcards),
        "input_lfns": input_lfns,
        "output_files": output_files,
        "common_tags": common_tags,
    }

    # HACK: Find the transformation ID
    assert len(rule_info["request_id"]) == 1, rule_info
    tids = {x.split("/")[5] for x in rule_info["input_lfns"]}
    assert len(tids) == 1
    rule_info["tid"] = tids.pop()

    if not hasattr(wrapper, "_metadata"):
        wrapper._metadata = {}
    key = (rule_info["snakefile"], rule_info["rule"], frozenset(rule_info["wildcards"].items()))
    if key in wrapper._metadata:
        raise NotImplementedError("This shouldn't happen")
    wrapper._metadata[key] = rule_info

    return pfns
