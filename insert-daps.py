#!/usr/bin/env python
import json
import hashlib
import tempfile
from datetime import datetime, timedelta
from pathlib import Path
from pprint import pprint

import DIRAC
DIRAC.initialize()

import gfal2
from DIRAC.Core.Utilities.File import generateGuid
from DIRAC.Core.Utilities.ReturnValues import returnValueOrRaise
from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient
from LHCbDIRAC.Core.Utilities.BookkeepingJobInfo import BookkeepingJobInfo
from LHCbDIRAC.TransformationSystem.Client.TransformationClient import TransformationClient
from DIRAC.DataManagementSystem.Client.DataManager import DataManager

metadata = json.loads(Path("metadata.json").read_text())
metadata2 = []
# for info in metadata[::-1]:
for info in metadata:
    name = info["wildcards"]["name"]
    input_tid = info["tid"]
    bk_metadata_path = Path("metdata") / f"{input_tid}.json"
    trans_meta = json.loads(bk_metadata_path.read_text())
    dap_version = "v1r1"
    dap_pass_name = f"dap-{dap_version}-recompress"

    # Get the metadata of the input transformation
    path1, path2 = trans_meta["Path"].strip().split("\n")
    filetype, _, eventtype, _ = trans_meta["Number of events"][0]
    assert path2 == f"{path1}/{eventtype}/{filetype}"
    configname, configversion, input_conddesc, input_procpass = path1.lstrip("/").split("/", 3)
    eventtype = f"{eventtype:08}"
    assert "//" not in input_procpass
    new_procpass = f"/{input_procpass}/{dap_pass_name}/"
    print([configname, configversion, input_conddesc, input_procpass, new_procpass, eventtype])

    # Create the transformation
    tc = TransformationClient()
    trans_name = f"DAP_{dap_version}_{name}"  # must be unique
    res = TransformationClient().getTransformation(trans_name)
    if res["OK"]:
        transform_id = res["Value"]["TransformationID"]
        print("Found exisiting transformation with ID", transform_id)
    else:
        transform_id = returnValueOrRaise(tc.addTransformation(trans_name, "description", "longDescription", "DerriviedAnalysisProduction", "LHCbDAPPlugin", "Manual", ""))
        print("Created transformation with ID", transform_id)

    # Add the transformation to the bookkeeping
    prod_info = {
        "Production": transform_id,
        "Steps": [],
        "EventType": eventtype,
        "ConfigName": configname,
        "ConfigVersion": configversion,
        "InputProductionTotalProcessingPass": new_procpass
    }
    assert "//" not in "InputProductionTotalProcessingPass"
    if configname == "LHCb":
        prod_info["DataTakingConditions"] = input_conddesc
    elif configname == "MC":
        prod_info["SimulationConditions"] = input_conddesc
    else:
        raise NotImplementedError()
    returnValueOrRaise(BookkeepingClient().addProduction(prod_info))

    # Generate the bookkeping XML
    version = info["wildcards"]["version"]
    assert len(info["output_files"]) == 1
    output_fn = Path(f"/eos/lhcb/wg/dpa/wp2/recompress-{version}/") / info["output_files"]["root_file"]

    start = datetime.utcnow() - timedelta(minutes=5)
    end = datetime.utcnow()
    parameters = {
        "configName": {"mc": "MC", "lhcb": "LHCb"}[info["common_tags"]["config"]],
        # "BKCondition"
        "eventType": info["common_tags"]["eventtype"],
    }
    if parameters["configName"] == "MC":
        parameters["configVersion"] = info["common_tags"]["datatype"]
    elif parameters["configName"] == "LHCb":
        parameters["configVersion"] = f"Collision{info['common_tags']['datatype'][2:]}"
    else:
        raise NotImplementedError("TODO")
    assert parameters["configName"] == configname, (parameters, configname)
    assert parameters["configVersion"] == configversion, (parameters, configversion)
    n_cpus = 1
    lbconda_env_name = "default"
    lbconda_env_version = "2023-04-26_20-20"
    output_filesize = output_fn.stat().st_size
    if output_filesize >= 290 * 1024**3:
        print("ERROR: File too large, skipping", output_filesize, output_fn)
        # continue
        raise NotImplementedError(f"TODO: {output_filesize}")
    merge_id = "00000001"
    output_lfn = f"/lhcb/{parameters['configName']}/{parameters['configVersion']}/{filetype}/{transform_id:08d}/0000/{transform_id:08d}_{merge_id}_1.{filetype.lower()}"
    res = returnValueOrRaise(BookkeepingClient().getFileMetadata(output_lfn))
    metadata2.append(info | {"output_lfns": [output_lfn], "trans_name": trans_name, "transform_id": transform_id})
    if output_lfn in res["Successful"]:
        print("Skipping LFN registration, already exists")
        print(res)
        continue

    with tempfile.NamedTemporaryFile() as tmp:
        ctx = gfal2.Gfal2Context()
        fh = ctx.open(f"root://eoslhcb.cern.ch/{output_fn}", "r")
        hasher = hashlib.md5()
        pos = 0
        while data := fh.read_bytes(100 * 1024**2):
            pos += len(data)
            hasher.update(data)
            tmp.write(data)
            print(f"At {pos/1024**2:.0f}MB")
        tmp.flush()
        output_md5 = hasher.hexdigest()
        output_guid = generateGuid(output_md5, "MD5")
        print("Finished, generated GUID:", output_guid)

        job_info = BookkeepingJobInfo(
            ConfigName=parameters["configName"],
            ConfigVersion=parameters["configVersion"],
            Date=end.strftime("%Y-%m-%d"),
            Time=end.strftime("%H:%M:%S"),
        )
        if simulation_condition := parameters.get("BKCondition"):
            job_info.simulation_condition = simulation_condition
        job_info.typed_parameters = BookkeepingJobInfo.TypedParameters(
            CPUTIME=f"{(end - start).total_seconds():.0f}",
            ExecTime=f"{(end - start).total_seconds() * n_cpus:.0f}",
            NumberOfProcessors=f"{n_cpus}",
            Production=f"{transform_id}",
            Name=f"{transform_id:08d}_{merge_id}_1",
            JobStart=start.strftime("%Y-%m-%d %H:%M"),
            JobEnd=end.strftime("%Y-%m-%d %H:%M"),
            Location=DIRAC.siteName(),
            ProgramName=f"lb-conda {lbconda_env_name}",
            ProgramVersion=lbconda_env_version,
            StepID="1",
            NumberOfEvents=f"{len(info['input_lfns'])}",
        )
        job_info.input_files = info["input_lfns"]
        job_info.output_files = [
            BookkeepingJobInfo.OutputFile(
                Name=output_lfn,
                TypeName=f"{filetype}",
                TypeVersion="1",
                EventTypeId=parameters["eventType"],
                EventStat=f"{len(info['input_lfns'])}",
                FileSize=output_filesize,
                CreationDate=end.strftime("%Y-%m-%d %H:%M:%S"),
                MD5Sum=output_md5,
                Guid=output_guid,
            ),
        ]
        pprint(info)
        bk_xml = job_info.to_xml().decode()
        print(bk_xml)

        if configname == "LHCb":
            targetSE = "CERN-DST-EOS"
        elif configname == "MC":
            targetSE = "CERN_MC-DST-EOS"
        else:
            raise NotImplementedError()

        # Register the file
        returnValueOrRaise(BookkeepingClient().sendXMLBookkeepingReport(bk_xml))
        res = returnValueOrRaise(DataManager().putAndRegister(output_lfn, tmp.name, targetSE, output_guid))
        if res["Failed"]:
            raise NotImplementedError(res)
        if {"put", "register"} - set(res["Successful"].get(output_lfn, [])):
            raise NotImplementedError(res)
        
Path("enriched_metadata.json").write_text(json.dumps(metadata2))


# request_id = -1

# sample = dict(
#    request_id=request_id,
#    name=sample["name"],
#    version="v0r1 (DerrivedAnalysisProductions)",
#    wg=sample["wg"],
#    analysis=sample["analysis"],
# )
# sample["validity_start"] = datetime.utcnow()
# sample["extra_info"] = {"snakemake_rule": []}
# # sample["extra_info"]["merge_request"] = url

# autoTags = getAutoTags(dataset["id"], pInfo["productionInputQuery"])
# sample["auto_tags"] = [dict(name=k, value=v) for k, v in autoTags.items()]
# requests.append(sample)


# returnValueOrRaise(tc.completeTransformation(tid))

# import subprocess
# import apd
# samples = apd.AnalysisData("rd", "lb2lll").all_samples()
# samples.tags
# sample = list(samples.filter(version="v0r0p5354949"))[0]
# tags = samples.tags["12460"]
# print(sample["wg"], sample["analysis"], sample["version"], sample["name"], list(sample["lfns"].items())[0], tags)
